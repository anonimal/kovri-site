# Kovri

Official Website for the Kovri Project

- https://kovri.io
- http://kovri.i2p

# Cloning

This repository requires a recursive clone (or subsequent initialization of the kovri-docs submodule):

```bash
$ git clone --recursive https://gitlab.com/kovri-project/kovri-site
```

If not cloned recursively:

```bash
$ cd kovri-site/ && git submodule init && git submodule update
```

# Vulnerability Response

- Our [Vulnerability Response Process](https://github.com/monero-project/meta/blob/master/VULNERABILITY_RESPONSE_PROCESS.md) encourages responsible disclosure
